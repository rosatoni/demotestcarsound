package cat.escolapia.pmdm;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class TestSound extends ApplicationAdapter {

	private SpriteBatch batch;
	private BitmapFont font;
	private OrthographicCamera camera;

	private Engine engine;

	@Override
	public void create() {
		batch = new SpriteBatch();

		font = new BitmapFont();
		font.setColor(Color.RED);
		font.getData().setScale(3f);

		camera = new OrthographicCamera(1024, 768);
		camera.position.set(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2, 0.0f);

		engine = new Engine();
	}

	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
		engine.dispose();
	}

	@Override
	public void render() {
		//PROCESS INPUT
		boolean accelerating = false;
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE) || Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
			accelerating = true;
		}
		if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) ) {
			if (engine.on) engine.off(); else engine.on();
		}

		//UPDATE
		engine.update(Gdx.graphics.getDeltaTime(), accelerating);

		//RENDER
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		if (engine.on) {
			font.setColor(Color.GREEN);
			Gdx.gl.glClearColor(0.6f, 0.6f, 0.6f, 1.0f);
		} else {
			font.setColor(Color.RED);
			Gdx.gl.glClearColor(0f, 0f, 0f, 1.0f);
		}
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			font.draw(batch, engine.speed + "km/h", 20.0f, 200.0f);
			if (engine.on) {
				font.draw(batch, "Space or Touch to accelerate.", 20.0f, 150.0f);
			}
			font.draw(batch, "Left Control to swtich on/off.", 20.0f, 100.0f);
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		batch.dispose();
		batch = new SpriteBatch();
		batch.setProjectionMatrix(camera.combined);
	}

}
