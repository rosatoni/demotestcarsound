package cat.escolapia.pmdm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

public class Engine {
        private final static float MAX = 200.0f;
        private final static float ACCELERATION = 25.0f;
        private final static float FRICTION = 15.0f;
        private final static float IDLE_THRESHOLD = 0.1f;

        public float speed = 0;
        public  boolean on = true;

        private Sound engineIdle;
        private Sound engineRunning;
        private long soundId;

        public Engine() {
            engineIdle = Gdx.audio.newSound(Gdx.files.internal("sound/engine-idle.wav"));
            engineRunning = Gdx.audio.newSound(Gdx.files.internal("sound/engine-running.wav"));
        }
        public void on() {
            soundId = engineIdle.play();
            engineIdle.setLooping(soundId, true);
            on = true;
        }
        public void off(){
            engineIdle.stop();
            engineRunning.stop();
            on = false;
        }
        public void update(float delta, boolean acceleration) {
            if (!on) {
                if (speed > IDLE_THRESHOLD) {
                    speed = speed - FRICTION * delta;
                    if (speed < IDLE_THRESHOLD) speed = 0;
                }
                return;
            }

            // SPEED
            boolean wasIdle = speed < IDLE_THRESHOLD;
            if (acceleration) {
                if (speed < MAX) {
                    speed = speed + ACCELERATION * delta;
                }
            } else {
                if (!wasIdle) speed = speed - FRICTION * delta;
            }
            boolean isIdle =  speed < IDLE_THRESHOLD;

            //SOUND
            if (wasIdle && !isIdle) {
                engineIdle.stop();
                soundId = engineRunning.play();
                engineRunning.setLooping(soundId, true);
            } else if (!wasIdle && isIdle) {
                speed = 0;
                engineRunning.stop();
                soundId = engineIdle.play();
                engineIdle.setLooping(soundId, true);
            }
            if (!isIdle) {
                float pitch = 0.5f + speed / MAX * 0.5f;
                engineRunning.setPitch(soundId, pitch);
            }
        }

        public void dispose() {
            engineIdle.dispose();
            engineRunning.dispose();
        }
    }